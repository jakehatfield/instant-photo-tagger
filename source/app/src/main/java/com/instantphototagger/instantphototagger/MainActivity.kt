package com.instantphototagger.instantphototagger

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.view.View
import android.widget.Toast
import android.util.Base64

import kotlinx.android.synthetic.main.activity_main.*

import org.json.JSONObject

import org.jetbrains.anko.doAsync

import java.text.SimpleDateFormat
import java.util.*
import java.io.*
import java.io.ByteArrayOutputStream
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {

    // flag alerts when camera is requested
    val CAMERA_REQUEST_CODE = 0
    lateinit var imageFilePath: String //lateinit lets me create the value later in program

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        useCameraButton.setOnClickListener {
            try {
                labelOutput.setText("Taking Photo...")
                val imageFile = createImageFile() // creates the file to use
                val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE) // creates bitmap preview
                if(callCameraIntent.resolveActivity(packageManager) != null) {
                    val authorities = packageName + ".fileprovider" //gets authorities from AndroidManifest provider
                    val imageUri = FileProvider.getUriForFile(this, authorities, imageFile)
                    callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri) //passes full size bit image to camera intent
                    startActivityForResult(callCameraIntent, CAMERA_REQUEST_CODE)
                }
            } catch (e: Exception) {
                Toast.makeText(this, "Could not create temp file!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            CAMERA_REQUEST_CODE -> {
                // Used to get bitmap thumbnail, use next if to get full bitmap
                //if(resultCode == Activity.RESULT_OK && data != null) {
                //    photoImageView.setImageBitmap(data.extras.get("data") as Bitmap)
                //}
                // Gets full sized bitmap image, but then run it through scalar
                if(resultCode == Activity.RESULT_OK) {
                    val photoToSend = setScaledBitmap()
                    photoImageView.setImageBitmap(photoToSend)
                    //send image to Google API asynchronously (must do this because call is on Main Thread)

                    labelOutput.setText("Retreiving image labels from Google...")

                    doAsync {
                        // resize to 640x480 as recommended size for bitmap per Google
                        //val resizedPhotoToSend = Bitmap.createBitmap(ph, 0, 0, 640, 480) //640x480 is recommended by Google //1280x960
                        // encode to Base64 string as required to send to Google
                        //val encodedImageString = encodeToBase64(resizedPhotoToSend, Bitmap.CompressFormat.WEBP, 100)

                        // condensed above variables into one function call, left above for example

                        sendPhotoByEncodedImageString(encodeToBase64(Bitmap.createBitmap(photoToSend, 0, 0, 640, 480), Bitmap.CompressFormat.WEBP, 100))
                    }
                }
            }
            else -> {
                Toast.makeText(this, "Unrecognized Request Code", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun sendPhotoByEncodedImageString(imageString: String)
    {
        val apiKey: String = BuildConfig.GoogleVisionApiKey
        val targetURL = "https://vision.googleapis.com/v1/images:annotate?key="
        val serverUrl = URL(targetURL + apiKey)
        val urlConnection = serverUrl.openConnection()
        val httpConnection = urlConnection as HttpURLConnection

        httpConnection.setRequestMethod("POST")
        httpConnection.setRequestProperty("Content-Type", "application/json")

        httpConnection.setDoOutput(true)

        val httpRequestBodyWriter = BufferedWriter(OutputStreamWriter(httpConnection.getOutputStream()))
        httpRequestBodyWriter.write("{\"requests\":[{\"features\":[{\"type\":\"LABEL_DETECTION\"}],\"image\":{\"content\":\"$imageString\"}}]}")
        httpRequestBodyWriter.close()

        httpConnection.getResponseMessage()

        //process response
        if (httpConnection.inputStream == null) {
            //labelOutput.setText("There are no labels for this image") //will create a views exception
            Toast.makeText(this, "No stream", Toast.LENGTH_LONG).show()
            return
        }

        val httpResponseScanner = Scanner(httpConnection.inputStream)
        var resp = ""
        while (httpResponseScanner.hasNext()) {
            val line = httpResponseScanner.nextLine()
            resp += line
            //println(line)  //  alternatively, print the line of response
        }

        httpResponseScanner.close()
        processResp(resp)

    }

    // takes the json response string from Google and gets the Labels from it
    // calls a method to display instead of returning
    fun processResp(resp: String)
    {
        // if there are no labels
        if(resp == ""){
            labelOutput.setText("There are no labels for this image.")
            return
        }

        // response json object is: {responses:{labelAnnotations:{ description }}} along with others
        //val jsonObj = JSONObject(resp)
        // condense jsonObject to labelAnnotations:{ description }}} along with others
        //val jsonObj2 = JSONObject(jsonObj.getJSONArray("responses").getString(0))

        //val labelAnnotations = jsonObj2.getJSONArray("labelAnnotations")

        //condensed above variables into one call
        val labelAnnotations = JSONObject(JSONObject(resp).getJSONArray("responses").getString(0)).getJSONArray("labelAnnotations")
        // instantiate empty array
        var labels = Array<String>(labelAnnotations.length(),{""})

        // loop through labelAnnotations and put descriptions into array
        for(i in 0..labelAnnotations.length() -1)
        {
            val item = labelAnnotations.getJSONObject(i)
            val temp = item.getString("description").toString()
            labels.set(i,temp)
        }

        // call function to display the labels on the screen
        runOnUiThread { displayLabels(labels) }
    }

    // takes an array of labels and displays them on the screen
    fun displayLabels(labels: Array<String>)
    {
        var outputString = ""

        if(labels.size == 0)
        {
            outputString = "There are no labels for this image"
        }
        else {
            for(i in 0..labels.size-1)
            {
                if(labels.get(i) != "")
                {
                    if(i == 0) {
                        outputString = "Image labels from Google: " + labels.get(i)
                        labelOutput.setText("Image labels from Google: " + labels.get(i))
                    }
                    else {
                        outputString = outputString + ", " + labels.get(i)
                        labelOutput.append(", " + labels.get(i))
                    }
                }
            }
        }

        labelOutput.setText(outputString)


        //labelOutput.invalidate()
        //labelOutput.refreshDrawableState()
        //recreate()
    }

    // tests button: used in initial setup
    fun takePhoto(view: View) {
        Toast.makeText(this, "Take Photo Button Pressed", Toast.LENGTH_LONG).show()
        //   var callCameraApplicationIntent = Intent.(MediaStore.ACTION_IMAGE_CAPTURE)
    }

    @Throws(IOException::class) //throws exception handled when creating temp file
    fun createImageFile(): File {
        val timestamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val imageFileName = "JPEG_" + timestamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        //if /Pictures doesn't exist, create it.
        if(!storageDir.exists()) storageDir.mkdirs() //mkdirs also creates parent folders if needed
        val imageFile = createTempFile(imageFileName, ".jpg", storageDir)
        imageFilePath = imageFile.absolutePath

        return imageFile
    }

    // returns the bitmap and shows it in the captured size for accurate display
    fun setScaledBitmap() :Bitmap {
        val imageViewWidth = photoImageView.width
        val imageViewHeight = photoImageView.height

        // gets bitmap dimensions
        val bitmapOptions = BitmapFactory.Options()
        bitmapOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(imageFilePath, bitmapOptions)
        val bitmapWidth = bitmapOptions.outWidth
        val bitmapHeight = bitmapOptions.outHeight

        // gets smallest number to keep ratio
        val scaleFactor = Math.min(bitmapWidth/imageViewWidth, bitmapHeight/imageViewHeight)
        bitmapOptions.inSampleSize = scaleFactor
        bitmapOptions.inJustDecodeBounds = false

        var orientation: Int = 0

        try {
            val ei = ExifInterface(imageFilePath)
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
        } catch (e: Exception) {
            Toast.makeText(this, "Orientation issue.", Toast.LENGTH_SHORT).show()
        }


        val matrix = Matrix()

        // if width < height, it is portrait, else landscape and doesn't need rotated

        // kotlin switch, rotates the image. Issue may be specific only to my phone: image from camera rotates when going to preview
        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> matrix.postRotate(90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> matrix.postRotate(180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> matrix.postRotate(270f)
        }

        val scaledBitmap = BitmapFactory.decodeFile(imageFilePath, bitmapOptions)

        // returns the bitmap with the options we just set and after rotation
        return Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
    }

}

// function to encode a bitmap into Base64 String
// this function is in use
fun encodeToBase64(image: Bitmap, compressFormat: Bitmap.CompressFormat, quality: Int): String {
    val byteArrayOS = ByteArrayOutputStream()
    image.compress(compressFormat, quality, byteArrayOS)
    return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT)
}

// function to decode a Base64 string to Bitmap
// this function is NOT used
fun decodeBase64(input: String): Bitmap {
    val decodedBytes = Base64.decode(input, 0)
    return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
}

// another function to encode a bitmap into Base64 String
// this function is NOT in use
fun convert(bitmap: Bitmap): String {
    val outputStream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
    return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
}
