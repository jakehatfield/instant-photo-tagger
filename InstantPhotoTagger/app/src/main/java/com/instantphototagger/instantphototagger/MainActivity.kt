package com.instantphototagger.instantphototagger

import android.app.Activity
import android.content.ContentResolver
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.Path
import android.media.ExifInterface
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStoragePublicDirectory
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.widget.Toast
import android.util.Base64

import kotlinx.android.synthetic.main.activity_main.*

import org.json.JSONObject

import org.jetbrains.anko.doAsync

import java.text.SimpleDateFormat
import java.util.*
import java.io.*
import java.io.ByteArrayOutputStream
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {

    // flag alerts when camera is requested
    private val CAMERA_REQUEST_CODE = 0
    private lateinit var imageFilePath: String //late init lets me create the value later in program

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // useCameraButton listener: Accesses camera to take a photo
        useCameraButton.setOnClickListener {
            try {
                labelOutput.text = "Taking Photo..."
                val imageFile = createImageFile() // creates the file to use
                val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE) // creates bitmap preview
                if(callCameraIntent.resolveActivity(packageManager) != null) {
                    val authorities = packageName + ".fileprovider" //gets authorities from AndroidManifest provider
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    val imageUri = FileProvider.getUriForFile(this, authorities, imageFile)

                    //val imageUri = Uri.fromFile(imageFile)
                    intent.putExtra(Intent.EXTRA_STREAM, imageUri)
                    callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri) //passes full size bit image to camera intent
                    startActivityForResult(callCameraIntent, CAMERA_REQUEST_CODE)
                }
            } catch (e: Exception) {
                labelOutput.text = "Could not create temp file! Please try again."
                Toast.makeText(this, "Could not create temp file!", Toast.LENGTH_SHORT).show()
            }
        }

        // renameImageButton listener: listens for the button to be clicked. renames the image and displays the google tag from the exif data
        renameImageButton.setOnClickListener {
            try {
                // get the value entered by the user for the new file name
                val newFileName = newImageFileName.text.toString() + ".jpg"
                // check if the file name entered was an empty string
                if(newFileName == ".jpg") {
                    Toast.makeText(this, "File name cannot be blank. Choose a different name and try again.", Toast.LENGTH_LONG).show()
                }
                // check if the file name starts with a '.'
                else if (newFileName.split(".")[0].isEmpty()) {
                    Toast.makeText(this, "File name cannot start with a \".\"! Choose a different name and try again.", Toast.LENGTH_LONG).show()
                }
                // file contains an invalid character
                else if (newFileName.contains("/")){
                    Toast.makeText(this, "File name cannot contain a \"/\"! Choose a different name and try again.", Toast.LENGTH_LONG).show()
                }
                // rename the file
                else {
                    // get current image filename from photoImageView
                    val imageFile = photoImageView.getTag(R.id.TAG_ONLINE_ID)
                    // get the directory path to the image
                    val storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM + "/Camera")

                    // create the new file name with the dir, so we can cast it as a File
                    val newFileNameWithDir = storageDir.toString() + "/" + newFileName
                    // check if the filename already exists
                    if (checkIfFileExists(newFileName)) {
                        //labelOutput.text = "File '" + newFileName + "' already exists. Choose a different name and try again."
                        Toast.makeText(this, "File '" + newFileName + "' already exists. Choose a different name and try again.", Toast.LENGTH_LONG).show()
                    }
                    // if everything appears valid, rename the file
                    else {
                        // rename the original file with the new filename
                        File(imageFile.toString()).renameTo((File(newFileNameWithDir)))
                        // place the new filename in the photoview so it can be renamed again if needed
                        photoImageView.setTag(R.id.TAG_ONLINE_ID, newFileNameWithDir)
                        //output success prompt
                        //labelOutput.text = "File renamed to " + newFileName + "."
                        Toast.makeText(this, "File renamed to " + newFileName + ".", Toast.LENGTH_SHORT).show()
                    }
                }

                // display the Google tag string
                // get the filename from the view tag
                var imageFile = photoImageView.getTag(R.id.TAG_ONLINE_ID)

                // cast the filename as a File
                imageFile = File(imageFile.toString())

                // get the uri from the File
                val uri = Uri.fromFile(imageFile)

                // create an InputStream from the uri
                val inStream  = contentResolver.openInputStream(uri)

                // create an ExifInterface with the InputStream
                val exif = ExifInterface(inStream)

                // get the Google tag from the exif tags
                val outputString = exif.getAttribute("ImageDescription")

                // if the tag is not empty or null, display it
                if(outputString != null && outputString != "") {
                    Toast.makeText(this, "Google Tags: " + outputString, Toast.LENGTH_SHORT).show()
                }
                // otherwise, show an alert/error message
                else {
                    Toast.makeText(this, "There are no tags for this image!", Toast.LENGTH_SHORT).show()
                }

            }
            // if the file couldn't be renamed, show an alert
            catch (e: Exception) {
                Toast.makeText(this, "Could not rename file or there is no file to rename!", Toast.LENGTH_LONG).show()
            }
        }
    }

    /*
    * checkFile function
    * Return true if file exists else false.
    * The storage path is hardcoded instead of a parameter because the path doesn't change
    * */
    private fun checkIfFileExists(fileName: String): Boolean {
        var result = false
        val storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM + "/Camera")
        val fileCheck = File(storageDir.toString() + "/" + fileName)
        if (fileCheck.exists())
            result = true
        return result
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode) {
            CAMERA_REQUEST_CODE -> {
                // Used to get bitmap thumbnail, use next if to get full bitmap
                //if(resultCode == Activity.RESULT_OK && data != null) {
                //    photoImageView.setImageBitmap(data.extras.get("data") as Bitmap)
                //}
                // Gets full sized bitmap image, but then run it through scalar
                if(resultCode == Activity.RESULT_OK) {
                    val photoToSend = setScaledBitmap()
                    photoImageView.setImageBitmap(photoToSend)
                    //send image to Google API asynchronously (must do this because call is on Main Thread)

                    labelOutput.text = "Retrieving image labels from Google..." //setText("Retrieving image labels from Google...")

                    doAsync {
                        // resize to 640x480 as recommended size for bitmap per Google
                        //val resizedPhotoToSend = Bitmap.createBitmap(ph, 0, 0, 640, 480) //640x480 is recommended by Google //1280x960
                        // encode to Base64 string as required to send to Google
                        //val encodedImageString = encodeToBase64(resizedPhotoToSend, Bitmap.CompressFormat.WEBP, 100)

                        // condensed above variables into one function call, left above for example

                        sendPhotoByEncodedImageString(encodeToBase64(Bitmap.createBitmap(photoToSend, 0, 0, 1280, 960), Bitmap.CompressFormat.JPEG, 100))
                    }
                } else {
                    labelOutput.text = "Please take a photo."
                }
            }
            else -> {
                labelOutput.text = "Please take a photo."
            }
        }
    }

    // sends a give photo encoded in Base64 to Google Vision Api for labels
    private fun sendPhotoByEncodedImageString(imageString: String)
    {
        val apiKey: String = BuildConfig.GoogleVisionApiKey
        val targetURL = "https://vision.googleapis.com/v1/images:annotate?key="
        val serverUrl = URL(targetURL + apiKey)
        val urlConnection = serverUrl.openConnection()
        val httpConnection = urlConnection as HttpURLConnection

        httpConnection.requestMethod = "POST" //setRequestMethod("POST")
        httpConnection.setRequestProperty("Content-Type", "application/json")

        httpConnection.doOutput = true // setDoOutput(true)

        val httpRequestBodyWriter = BufferedWriter(OutputStreamWriter(httpConnection.outputStream))
        httpRequestBodyWriter.write("{\"requests\":[{\"features\":[{\"type\":\"LABEL_DETECTION\"}],\"image\":{\"content\":\"$imageString\"}}]}")
        httpRequestBodyWriter.close()

        httpConnection.responseMessage // getResponseMessage()

        //process response
        if (httpConnection.inputStream == null) {
            //labelOutput.setText("There are no labels for this image") //will create a views exception
            Toast.makeText(this, "No stream", Toast.LENGTH_LONG).show()
            return
        }

        val httpResponseScanner = Scanner(httpConnection.inputStream)
        var resp = ""
        while (httpResponseScanner.hasNext()) {
            val line = httpResponseScanner.nextLine()
            resp += line
            //println(line)  //  alternatively, print the line of response
        }

        httpResponseScanner.close()
        processResp(resp)

    }

    // takes the json response string from Google and gets the Labels from it
    // calls a method to display instead of returning
    private fun processResp(resp: String)
    {
        // if there are no labels
        if(resp == ""){
            labelOutput.text = "There are no labels for this image." // setText("There are no labels for this image.")
            return
        }

        // response json object is: {responses:{labelAnnotations:{ description }}} along with others
        //val jsonObj = JSONObject(resp)
        // condense jsonObject to labelAnnotations:{ description }}} along with others
        //val jsonObj2 = JSONObject(jsonObj.getJSONArray("responses").getString(0))

        //val labelAnnotations = jsonObj2.getJSONArray("labelAnnotations")

        //condensed above variables into one call
        val labelAnnotations = JSONObject(JSONObject(resp).getJSONArray("responses").getString(0)).getJSONArray("labelAnnotations")
        // instantiate empty array
        val labels = Array(labelAnnotations.length(),{""})
        val scores = Array(labelAnnotations.length(),{0f})

        // loop through labelAnnotations and put descriptions into array
        for(i in 0 until labelAnnotations.length() -1)
        {
            val item = labelAnnotations.getJSONObject(i)
            val temp = item.getString("description").toString()
            labels[i] = temp //.set(i,temp)
            val temp2 = item.getString("score").toFloat()
            scores[i] = temp2
        }

        // call function to display the labels on the screen
        runOnUiThread { displayLabels(labels,scores) }
    }

    // takes an array of labels and displays them on the screen
    private fun displayLabels(labels: Array<String>, scores: Array<Float>)
    {
        var outputString = ""

        if(labels.isEmpty())
        {
            outputString = "There are no labels for this image!"
        }
        else {
            for(i in 0 until labels.size-1)
            {
                if(labels[i] != "")
                {
                    val scoreInPercent = scores[i] * 100 // turn confidence values into percentages and format them to 2 decimal places
                    // first iteration, create string with heading
                    if(i == 0) {
                        outputString = "Image labels from Google: " + labels[i] + "(" + ("%.2f".format(scoreInPercent)) + "%)"
                        labelOutput.text = outputString
                    }
                    // multiple iterations, append string with a preceding comma
                    else {
                        outputString = outputString + ", " + labels[i]  + "(" + ("%.2f".format(scoreInPercent)) + "%)"
                        labelOutput.append(", " + labels[i] + "(" + ("%.2f".format(scoreInPercent)) + "%)")
                    }
                }

            }
        }

        labelOutput.text = outputString

        saveExifTag(outputString)
    }

    // takes a string (specifically the tag string) and stores it in the exif ImageDescription tag
    private fun saveExifTag(tagString:String)
    {
        val imageFile = photoImageView.getTag(R.id.TAG_ONLINE_ID)

        val ei = ExifInterface(imageFile.toString())

        // ImageDescription is preexisting tag that can be used to save the tags
        ei.setAttribute("ImageDescription", tagString)
        ei.saveAttributes()
    }

    /*// tests button: used in initial setup
    fun takePhoto(view: View) {
        Toast.makeText(this, "Take Photo Button Pressed", Toast.LENGTH_LONG).show()
        //   var callCameraApplicationIntent = Intent.(MediaStore.ACTION_IMAGE_CAPTURE)
    }*/

    @Throws(IOException::class) //throws exception handled when creating temp file
    private fun createImageFile(): File {
        val timestamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date())
        val imageFileName = "JPEG_" + timestamp + "_"

        //val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM + "/Camera")

        val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        ActivityCompat.requestPermissions(this, permissions,0)

        //if directory doesn't exist, create it.
        if(!storageDir.exists()) storageDir.mkdirs() //mkdirs also creates parent folders if needed
        val imageFile = createTempFile(imageFileName, ".jpg", storageDir)

        imageFilePath = imageFile.absolutePath

        photoImageView.setTag(R.id.TAG_ONLINE_ID, imageFile)

        return imageFile
    }

    // returns the bitmap and shows it in the captured size for accurate display
    private fun setScaledBitmap() :Bitmap {
        val imageViewWidth = photoImageView.width
        val imageViewHeight = photoImageView.height

        // gets bitmap dimensions
        val bitmapOptions = BitmapFactory.Options()
        bitmapOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(imageFilePath, bitmapOptions)
        val bitmapWidth = bitmapOptions.outWidth
        val bitmapHeight = bitmapOptions.outHeight

        // gets smallest number to keep ratio
        val scaleFactor = Math.min(bitmapWidth/imageViewWidth, bitmapHeight/imageViewHeight)
        bitmapOptions.inSampleSize = scaleFactor
        bitmapOptions.inJustDecodeBounds = false

        var orientation = 0

        try {
            val ei = ExifInterface(imageFilePath)
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
        } catch (e: Exception) {
            Toast.makeText(this, "Orientation issue.", Toast.LENGTH_SHORT).show()
        }


        val matrix = Matrix()

        // if width < height, it is portrait, else landscape and doesn't need rotated

        // kotlin switch, rotates the image. Issue may be specific only to my phone: image from camera rotates when going to preview
        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> matrix.postRotate(90f)
            ExifInterface.ORIENTATION_ROTATE_180 -> matrix.postRotate(180f)
            ExifInterface.ORIENTATION_ROTATE_270 -> matrix.postRotate(270f)
        }

        val scaledBitmap = BitmapFactory.decodeFile(imageFilePath, bitmapOptions)

        // returns the bitmap with the options we just set and after rotation
        return Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.width, scaledBitmap.height, matrix, true)
    }

}

// function to encode a bitmap into Base64 String
// this function is in use
fun encodeToBase64(image: Bitmap, compressFormat: Bitmap.CompressFormat, quality: Int): String {
    val byteArrayOS = ByteArrayOutputStream()
    image.compress(compressFormat, quality, byteArrayOS)
    return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT)
}

/*// function to decode a Base64 string to Bitmap
// this function is NOT used
fun decodeBase64(input: String): Bitmap {
    val decodedBytes = Base64.decode(input, 0)
    return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
}*/

/*// another function to encode a bitmap into Base64 String
// this function is NOT in use
fun convert(bitmap: Bitmap): String {
    val outputStream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
    return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
}*/