# Instant Photo Tagger

## Basic Concept
The Instant Photo Tagger is designed for marketers that are concerned about the tags Google gives their photos. This application will allow you to search for the tags that you want and make sure they are in your photo. If they aren't, you don't have to save the image and you are free to take another knowing that you can get the tags you require.

This project's base model is a Samsung Galaxy S6 running on Nougat 7.0.

I am managing this project using Microsoft's Visual Studios Team Services. You can visit my page [here](https://revolutionai.visualstudio.com/Instant%20Photo%20Tagger/_dashboards?activeDashboardId=9a1043ed-7a38-457a-8c4c-64acd990c773).

## About Me
I am Jake Hatfield and I am a senior at Western Oregon University studying
Computer Science and I will graduate in June, 2018. This project is one of two that I am working on for my capstone projects for Computer Science. My other project is a group project and you can find it [here](https://bitbucket.org/devonsmith/senior-project/ "Jake's Group Project on BitBucket").

## Vision Statement
For online marketers who need their photos to have a specific description tag, the 'Instant Photo Tagger' is a photo analyzing product that analyzes photos for description tags. Unlike posting a photo online without knowing what the description tags are, my product will analyze your photo and display the description tags while you are on-site so you can ensure your photo has the description tag you desire.

## Needs and Features
* Capture photos with my cell phone's camera.
* Preview image before I push the button to take the photo
* View the tags that are given to the photo by Google.
* View the tags immediately so I can take another photo if necessary.
* Save accepted photos to existing file structure.
* Have an easy to use interface
* Ability to display the tags while seeing a preview of the image
* Ability to name the file? before saving
* Ability to not save the image (easily remove and take a new photo)

## Requirements
* Cell phone with Camera capabilities
* Cell phone model have OS Nougat 7.0 or compatible

## Non-Functional Requirements
* Internet connection
* Must integrate with existing file structure
* Coding in Kotlin

## Architecture Diagram
![Architecture Diagram](/images/ArchitectureDiagram.JPG)

## Use-Case Diagram
![Use-Case Diagram](/images/UseCaseDiagram.JPG)
